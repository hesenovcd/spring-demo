package spring.framework.demo.webflux.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import spring.framework.demo.webflux.config.MongoConfiguration;
import spring.framework.demo.webflux.entity.Employee;
import spring.framework.demo.webflux.repo.EmployeeRepository;

@RestController
@RequestMapping("/employees")
@RequiredArgsConstructor
public class EmployeeController {
    private final EmployeeRepository employeeRepository;

    @GetMapping
    public Flux<Employee> getAllEmployees() {
        Flux<Employee> all = employeeRepository.findAll();
        all.log().subscribe(System.out::println);
        return all;
    }

    @GetMapping("/{id}")
    public Mono<Employee> getEmployeeById(@PathVariable String id) {
        return employeeRepository.findById(id);
    }
}
