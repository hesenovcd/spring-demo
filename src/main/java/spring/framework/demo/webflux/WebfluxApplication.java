package spring.framework.demo.webflux;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import spring.framework.demo.webflux.entity.Employee;
import spring.framework.demo.webflux.repo.EmployeeRepository;

import java.time.LocalDate;
import java.util.stream.IntStream;

@SpringBootApplication
@EnableMongoRepositories
public class WebfluxApplication {

    @Autowired
    private EmployeeRepository employeeRepository;

    public static void main(String[] args) {
        SpringApplication.run(WebfluxApplication.class, args);
    }

    @EventListener(ApplicationReadyEvent.class)
    public void applicationStart() {
        if (employeeRepository.count().block() == 0) {
            IntStream.range(0, 100)
                    .mapToObj(this::generate)
                    .map(employeeRepository::save)
                    .forEach(item -> item.subscribe(
                            System.out::println,
                            System.err::println)
                    );
        }
    }

    private Employee generate(int i) {
        return Employee.builder()
                .name("Name" + i)
                .lastName("LastName" + i)
                .birthDate(LocalDate.now())
                .build();
    }

}
