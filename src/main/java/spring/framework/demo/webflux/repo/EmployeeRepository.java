package spring.framework.demo.webflux.repo;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import spring.framework.demo.webflux.entity.Employee;

public interface EmployeeRepository extends ReactiveMongoRepository<Employee, String> {

}
