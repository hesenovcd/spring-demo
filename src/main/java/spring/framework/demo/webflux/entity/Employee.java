package spring.framework.demo.webflux.entity;

import lombok.Builder;
import lombok.Data;
import lombok.Locked;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.time.LocalDate;

@Document
@Builder
@ToString
@Data
public class Employee {

    @Id
    private int id;
    private String name;
    private String lastName;
    private LocalDate birthDate;
}
