package spring.framework.demo.streams;

import spring.framework.demo.model.Person;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class StreamExamples {

    private static final HashMap<String, String> PHONE_NUMBERS = new HashMap<String, String>() {
        {
            put("Cavid","0559925244");
            put("Camal","0554585080");
            put("Dede","0506894266");
        }
    };

    public static void main(String[] args) {

        List<Person> personList = new ArrayList<>();
        personList.add(new Person("dads", 15));
        personList.add(new Person("dfgdfgf", 25));
        personList.add(new Person("duioiuoads", 19));
        personList.add(new Person("landja", 13));

        List<Integer> intList = new ArrayList<>();
        intList.add(1);
        intList.add(2);
        intList.add(55);
        intList.add(46);

        System.out.println(getStringFromMap("0559925244").get());
    }


    public List<String> mapToUppercase(String... names) {
        return Arrays.stream(names)
                .map(String::toUpperCase)
                .collect(Collectors.toList());
    }


    public static int getTotalSumOfLengthMoreThan5(String... names) {
        return Arrays.stream(names)
                .filter(name -> name.length() > 5)
                .mapToInt(String::length)
                .sum();
    }

    public static List<String> getFlatList(List<List<String>> colection) {
        return colection.stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toList());
    }

    public Person getMaxAgePerson(List<Person> people) {
        return people.stream()
                .max(Comparator.comparing(Person::getAge))
                .orElse(null);
    }

    public int getSumAndReduce(List<Integer> numbers) {
        return numbers.stream()
                .reduce(Integer::sum)
                .orElse(0);
    }

    public static Map<Boolean, List<Person>> getPersonMapByAge(List<Person> people) {
        return people.stream()
                .collect(Collectors.groupingBy(person -> person.getAge() >= 18, Collectors.toList()));
    }

    public static String nameToString(List<Person> people) {
        return "Names " + people.stream()
                .map(Person::getName)
                .reduce((i, sum) -> i + ", " + sum)
                .orElse("") + ".";
    }

    public static String nameToString2(List<Person> people) {
        return "Names " + people.stream()
                .map(Person::getName)
                .collect(Collectors.joining(","))
                + ".";
    }

    public static String getString(List<Integer> numbers) {
        return numbers.stream()
                .map(i -> i % 2 == 0 ? "e" + i : "o" + i)
                .collect(Collectors.joining(","));
    }

    public static Optional<String> getStringFromMap(String phoneNumber) {
       return PHONE_NUMBERS.entrySet().stream()
                .filter(entry -> entry.getValue().equals(phoneNumber))
                .map(Map.Entry::getKey)
                .findFirst();
    }

}
