package spring.framework.demo.controller;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import spring.framework.demo.entity.User;
import spring.framework.demo.service.UserService;

import java.util.List;

@RestController
@RequestMapping("/mongo-users")
@RequiredArgsConstructor
public class UserMongoController {

    private final UserService service;

    @PostConstruct
    public void init(){
        User user = new User();
        user.setName("Cavid");
        user.setSurname("Hasanov");
        service.save(user);
    }

    @PostMapping("/add")
    public ResponseEntity<User> add(@RequestBody User user) {
        return ResponseEntity.ok(service.save(user));
    }

    @GetMapping("/find/all")
    public ResponseEntity<List<User>> findAll() {
        return ResponseEntity.ok(service.findAll());
    }

}
