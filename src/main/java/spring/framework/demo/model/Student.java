package spring.framework.demo.model;

import lombok.AllArgsConstructor;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Objects;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Stack;
import java.util.Vector;

@AllArgsConstructor
public class Student {
    private int id;
    private String name;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return id == student.id && Objects.equals(name, student.name);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name);
    }

    public static void main(String[] args) {
        Deque queue = new ArrayDeque();
        queue.peekFirst();
    }


}
