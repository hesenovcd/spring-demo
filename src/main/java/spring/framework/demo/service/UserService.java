package spring.framework.demo.service;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import spring.framework.demo.entity.User;
import spring.framework.demo.repository.UserRepository;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public User save(User user) {
        return userRepository.save(user);
    }

    public List<User> findAll() {
        return userRepository.findAll();
    }

}
