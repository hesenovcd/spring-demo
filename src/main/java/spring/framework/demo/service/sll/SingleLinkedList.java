package spring.framework.demo.service.sll;

public class SingleLinkedList {

    public Node head = null;
    public Node tail = null;
    public int size = 0;

    public Node createSingleLinkedList(int nodeValue) {
        head = new Node();
        Node node = new Node();
        node.next = null;
        node.value = nodeValue;
        head = node;
        tail = node;
        size = 1;
        return head;
    }


    public void insertLinkedList(int nodeValue, int index) {
        Node node = new Node();
        node.value = nodeValue;
        if (head == null) {
            head = createSingleLinkedList(nodeValue);
        } else if (index == 0) {
            node.next = head;
            head = node;
        } else if (index >= size) {
            node.next = null;
            tail.next = node;
            tail = node;
        } else {
            Node tempNode = head;
            int i = 0;
            while (i < index - 1) {
                tempNode = tempNode.next;
                i++;
            }
            Node nextNode = tempNode.next;
            tempNode.next = node;
            node.next = nextNode;
        }
        size++;
    }

    public void traverseLinkedList() {
        if (head == null) {
            System.out.print("SLL does not exist");
        } else {
            Node tempNode = head;
            for (int i = 0; i < size; i++) {
                System.out.print(tempNode.value);
                if (i != size - 1) {
                    System.out.print("->");
                }
                tempNode = tempNode.next;
            }
        }
        System.out.println("\n");
    }

    public boolean searchLinkedList(int nodeValue) {
        if (head != null) {
            Node currentNode = head;
            for (int i = 0; i < size; i++) {
                if (currentNode.value == nodeValue) {
                    System.out.println("Node found at index " + i);
                    return true;
                }
                currentNode = currentNode.next;
            }
        }
        System.out.println("Node not found");
        return false;
    }

    public void deleteLinkedList(int index) {
        if (head == null) {
            System.out.println("SLL does not exist");
            return;
        } else if (index == 0) {
            head = head.next;
            size--;
            if (size == 0) {
                tail = null;
            }
        } else if (index >= size) {
            Node tempNode = head;
            for (int i = 0; i < size - 1; i++) {
                tempNode = tempNode.next;
            }
            if (tempNode == head) {
                tail = head = null;
                size--;
                return;
            }
            tempNode.next = null;
            tail = tempNode;
            size--;
        } else {
            Node tempNode = head;
            for (int i = 0; i < index - 1; i++) {
                tempNode = tempNode.next;
            }
            tempNode.next = tempNode.next.next;
            size--;
        }
    }

    public void deleteSll(){
        head=null;
        tail=null;
        System.out.println("SLL deleted successfully");
    }
}
