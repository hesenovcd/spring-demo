package spring.framework.demo.service.csll;

public class Main {

    public static void main(String[] args) {
        CircularSingleLinkedList csll = new CircularSingleLinkedList();
        csll.createCsll(5);
        csll.insertCsll(4, 0);
        csll.insertCsll(6, 1);
        csll.insertCsll(7, 8);
        csll.traverseCsll();
        System.out.println("\n");
        csll.searchNode(7);
    }
}
