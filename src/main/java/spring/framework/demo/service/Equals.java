package spring.framework.demo.service;

import spring.framework.demo.model.Student;

import java.util.HashMap;
import java.util.Map;

public class Equals {

    public static void main(String[] args) {
        Student student = new Student(1,"Cavid");

        Map<Student,Integer> map = new HashMap<>();
        map.put(new Student(1,"Cavid"),1);

        System.out.println(map.get(new Student(1,"Cavid")));

    }
}
