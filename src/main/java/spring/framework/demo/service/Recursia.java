package spring.framework.demo.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Recursia {


//    public static int factorial(int n) {
//        if (n <= 0 || n == 1) {
//            return 1;
//        }
//        return n * factorial(n - 1);
//    }


    public static void main(String[] args) {
        List<Integer> ints = new LinkedList<>();
        long start = System.currentTimeMillis();
        System.out.println("Started :" + start);
        for (int i = 0; i < 10000; i++) {
            ints.add(i);
        }//19 milllisecond
        System.out.println("size="+ints.size());
        System.out.println("Elapsed time writing :" + (System.currentTimeMillis() - start));
        Integer i1 = 0;
        for (int i = 0; i < ints.size(); i++) {
            i1 = ints.get(i);
        }//28-34
        System.out.println(i1);
        System.out.println("Elapsed time full :" + (System.currentTimeMillis() - start));
    }


    public static void reverseArray(int[] a) {
        System.out.println("Length: " + a.length);
        for (int i = 0; i < a.length / 2; i++) { //1  {//1, 3, 4, 5, 7,
            int temp = a[i];//3
            a[i] = a[a.length - i - 1];//12
            a[a.length - i - 1] = temp;//1
        }
        System.out.println(Arrays.toString(a));
    }

    public static void printBothArrayElements() {
        int[] arr = {1, 3, 4, 5};
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j < arr.length; j++) {
                System.out.println(arr[i] + " " + arr[j]);
            }

        }
    }

    public static void printUnorderedArrayElements(int[] a, int[] b) {

        for (int i = 0; i < a.length; i++) {
            for (int j = 0; j < b.length; j++) {
                if (a[i] < b[j]) {
                    System.out.println(a[i] + " " + b[j]);
                }
            }
        }
    }

//    private static void getArrayList() {
//        List<Integer> a = new ArrayList<>();
//        long started = System.currentTimeMillis();
//        System.out.println("Start length =" + a.size());
//        for (int i = 0; i < 1000000; i++) {
//            a.add(1, i);
//            a.remove(i);
//        }
//        System.out.println("Difference :" + (System.currentTimeMillis() - started));
//        System.out.println("Finish length =" + a.size());
//       // List
//    }
//
//    private static void getArray() {
//        int[] a = new int[1000000];
//        long started = System.currentTimeMillis();
//        System.out.println("Start length =" + a.length);
//        for (int i = 0; i < a.length; i++) {
//            a[i] = i;
//            //System.out.println(i);
//        }
//        System.out.println("Difference :" + (System.currentTimeMillis() - started));
//        System.out.println("Finish length =" + a.length);
//    }
//
//    public static int getSum(int n) {
//        if (n <= 0) {
//            return 0;
//        }
//        return getSum(n - 1) + n;
//    }

}
